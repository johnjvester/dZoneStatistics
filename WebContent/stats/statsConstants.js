angular.module('dZoneStatistics.stats')
    .constant("GLOBAL", {
        "URL_BASE": "/services/widget/article-listV2/list?author=",
        "URL_LIKE_BASE": "/services/internal/data/dzone-likeContext?node=",
        "URL_COMMENT_BASE": "/services/internal/data/articles-getActivityStatus?article=",
        "URL_PAGE": "&page=",
        "URL_PORTAL_SORT": "&portal=all&sort=newest",
        "URL_VIEW_BASE": "https://dzone.com"
    });
