angular.module('dZoneStatistics.stats')
    .service("statsService", ['GLOBAL', '$http', '$q',
        function (GLOBAL, $http, $q) {
            'use strict';

            var service = {};

            service.getLikeCount = function(id) {
                return $http.get(GLOBAL.URL_VIEW_BASE + GLOBAL.URL_LIKE_BASE + id)
                    .then(function (response) {
                        if (response && response.data.result && response.data.result.data && response.data.result.data.score) {
                            // Sample Response, which is wrapped in an outer `data` object:
                            // {"success":true,"result":{"data":{"liked":false,"canLike":true,"score":15}},"status":200}

                            return response.data.result.data.score;
                        }

                        return 0;
                    });
            };

            service.getCommentCount = function(id) {
                return $http.get(GLOBAL.URL_VIEW_BASE + GLOBAL.URL_COMMENT_BASE + id)
                    .then(function (response) {
                        // Sample Response, which is wrapped in an outer `data` object:
                        // {"success":true,"result":{"data":{"comments":1,"isDeleted":false,"saved":false,"liked":false}},"status":200}

                        if (response && response.data && response.data.result && response.data.result.data.comments) {
                            return response.data.result.data.comments;
                        }

                        return 0;
                    });
            };

            service.getAllDzonePages = function (id, pages) {
                var promiseArray = [];
                var resultsArray = [];
                var pagesWithData = [];

                angular.forEach(pages, function (page) {
                    var deferred = $q.defer();
                    promiseArray.push(deferred.promise);
                    console.log('page=' + page);

                    $http.get(GLOBAL.URL_VIEW_BASE + GLOBAL.URL_BASE + id + GLOBAL.URL_PAGE + page + GLOBAL.URL_PORTAL_SORT)
                    .then(function (response) {
                        if (response.data.result.data.nodes.length > 0) {
                            for (var i = 0; i < response.data.result.data.nodes.length; i++) {
                                // Introduce new properties to track comments and likes.
                                response.data.result.data.nodes[i].comments = 0;
                                response.data.result.data.nodes[i].likes = 0;

                                if (response.data.result.data.nodes[i].views === 0) {
                                    console.warn('Skip adding "' + response.data.result.data.nodes[i].title + '" (id=' + response.data.result.data.nodes[i].id + ') to resultsArray. Zero page views.');
                                } else {
                                    resultsArray.push(response.data.result.data.nodes[i]);
                                    console.log('resultsArray.length=' + resultsArray.length);
                                }
                            }

                            pagesWithData.push(page);
                        }

                        setTimeout(function () {
                            deferred.resolve();
                        }, 1000);
                    });
                });

                var deferredResults = $q.defer();
                $q.all(promiseArray).then(function(results) {
                    if (pages.length == pagesWithData.length) {
                        console.warn('Warning!!! Processing ' + pages.length + ' pages, which has no buffer space.  Consider increasing the `pages` value.');
                    } else {
                        console.info('Processing ' + pages.length + ' pages and only using ' + pagesWithData.length + ' pages.');
                    }
                    
                    console.log("resultsArray", resultsArray);
                    deferredResults.resolve(resultsArray);
                });

                return deferredResults.promise;
            };

            return service;
        }]);