angular.module('dZoneStatistics.stats')
    .controller("statsCtrl", ['statsService', '$location', 'GLOBAL',
        function (statsService, $location, GLOBAL) {
            'use strict';

            var vm = this;
            vm.data = [];
            vm.articleCount = 0;
            vm.totalPageViews = 0;
            vm.averagePageViewsPerArticle = 0;
            vm.views = [];
            vm.names = [];
            vm.realName = "";
            vm.avatarURL = "";
            vm.averageViewsPerDay = 0;
            vm.daysSinceFirstPost = 1;
            vm.firstArticlePubDate = moment();
            vm.baseURL = GLOBAL.URL_VIEW_BASE;

            // TODO: move code to service
            // TODO: check for max records and throw warning
            // TODO: hop out when zero records returned
            // TODO: look into pushing to AWS free
            // TODO: better way to get user info (instead of first article, first author)

            var today = moment();

            function getDzoneAllPageInfo(id, maxPageNum) {
                vm.data = [];
                var pages = [];

                for (var i = 1; i <= maxPageNum; i++) {
                    pages[i-1] = i;
                }

                statsService.getAllDzonePages(id, pages).then(function (response) {
                    if (response) {
                        vm.data = response;
                        vm.articleCount = vm.data.length;
                        getLikesAndCommentsData();
                        getPageViewStats();
                        computeFinalStats();
                        getNameAndAvatar(response[0]);
                    }
                });
            }

            function getLikesAndCommentsData() {
                vm.data.forEach(function (article) {
                    statsService.getLikeCount(article.id).then(function (likes) {
                        if (likes) {
                            article.likes = likes;
                        } else {
                            article.likes = 0;
                        }
                    });

                    statsService.getCommentCount(article.id).then(function (comments) {
                        if (comments) {
                            article.comments = comments;
                        } else {
                            article.comments = 0;
                        }
                    });
                });

            }

            function getNameAndAvatar(data) {
                vm.realName = data.authors[0].realName;
                vm.avatarURL = "https://" + data.authors[0].avatar;
            }

            function getPageViewStats() {
                var viewTotal = 0;

                vm.data.forEach(function (article) {
                    determineOldestArticle(article);
                    viewTotal = viewTotal + article.views;
                });

                vm.totalPageViews = viewTotal;

                if (viewTotal > 0) {
                    vm.averagePageViewsPerArticle = viewTotal / vm.data.length;
                }
            }

            function determineOldestArticle(thisData) {
                var thisArticleDate = moment(thisData.articleDate);

                if (moment(thisArticleDate).isBefore(vm.firstArticlePubDate)) {
                    vm.firstArticlePubDate = thisArticleDate;
                }
            }

            function computeFinalStats() {
                vm.daysSinceFirstPost = today.diff(vm.firstArticlePubDate, "days");
                vm.averageViewsPerDay = vm.totalPageViews / vm.daysSinceFirstPost;
            }

            function init() {
                var id = $location.search().id;

                if (!id) {
                    // if no id,
                    id = 770327;
                }

                getDzoneAllPageInfo(id, 50);
            }

            init();
    }]);