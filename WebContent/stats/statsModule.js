'use strict';

angular.module('dZoneStatistics.stats', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/stats', {
            templateUrl: 'stats/stats.html',
            controller: 'statsCtrl',
            controllerAs: 'ctrl'
        });
    }]).filter('moment', function() {
    return function(dateString, format) {
        return moment(dateString).format(format);
    };
})